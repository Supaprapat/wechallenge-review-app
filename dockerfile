FROM solr:8.6.2

ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar
COPY datafile/* ./
COPY script/solr_script.sh .
COPY entrypoint.sh .
ENTRYPOINT ["./entrypoint.sh"]