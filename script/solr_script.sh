#!/bin/bash
#docker run -d -p 8983:8983 -t solr
if [ -f executed ]; then
   exit 0
fi
solr create -c Review
solr create -c Food_dictionary

curl -X POST -H 'Content-type:application/json' --data-binary '{
  "add-field":{
     "name":"reviewID",
     "type":"string",
     "stored":true,
     "multiValued": false }
}' http://localhost:8983/solr/Review/schema

curl -X POST -H 'Content-type:application/json' --data-binary '{
  "add-field":{
     "name":"review",
     "type":"text_general",
     "stored":true,
     "multiValued": false }
}' http://localhost:8983/solr/Review/schema

curl 'http://localhost:8983/solr/Review/update?commit=true&separator=%3B&fieldnames=id,review' --data-binary @test_file.csv -H 'Content-type:application/csv'

curl -X POST -H 'Content-type:application/json' --data-binary '{
  "add-field":{
     "name":"word",
     "type":"text_general",
     "stored":true,
     "multiValued": false }
}' http://localhost:8983/solr/Food_dictionary/schema

curl 'http://localhost:8983/solr/Food_dictionary/update?commit=true&fieldnames=word' --data-binary @food_dictionary_top20k.txt -H 'Content-type:application/csv'

touch executed