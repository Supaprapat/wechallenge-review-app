function getReviewById(id) {
    fetch('http://localhost:5555/reviews/' + id)
      .then(response => response.json())
      .then(data => showOneReview(data));
}

function showOneReview(review) {
  const reviewTextareaElement = document.querySelector('#oneReviewTextarea');
  reviewTextareaElement.value = `${review.review}`;
}

function editReview(id, reviewText) {
    const data = { review: `${reviewText}` };

    fetch('http://localhost:5555/reviews/' + id, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    })
    .then(response => response.json())
    .then(data => {
      console.log('Success:', data);
      showOneReview(data);
    })
    .catch((error) => {
      console.error('Error:', error);
    });
}

function searchReviewsByKeyword(keyword) {
    fetch('http://localhost:5555/reviews?q=' + keyword)
      .then(response => response.json())
      .then(data => showReviews(data));
}

function showReviews(reviews) {
    const showReviewsDiv = document.querySelector('#showReviews');
    showReviewsDiv.innerHTML = "";
    reviews.forEach(review => {
      const reviewElement = document.createElement('p');
      reviewElement.innerText = `Review: ${review.review}`;
      showReviewsDiv.append(reviewElement);
    });
}