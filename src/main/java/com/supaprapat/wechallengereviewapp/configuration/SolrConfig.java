package com.supaprapat.wechallengereviewapp.configuration;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.solr.core.SolrOperations;
import org.springframework.data.solr.core.SolrTemplate;
import org.springframework.data.solr.repository.config.EnableSolrRepositories;

@Configuration
@EnableSolrRepositories(basePackages = "com.supaprapat.wechallengereviewapp.repository")
public class SolrConfig {

    @Value("${solr.url}")
    private String solrUrl;

    @Bean
    public SolrClient solrClient(String solrUrl) {
        return new HttpSolrClient.Builder(solrUrl).build();
    }

    @Bean
    public SolrOperations solrTemplate() {
        return new SolrTemplate(solrClient(solrUrl));
    }
}
