package com.supaprapat.wechallengereviewapp.controller;

import com.supaprapat.wechallengereviewapp.exception.NotFoundException;
import com.supaprapat.wechallengereviewapp.model.Review;
import com.supaprapat.wechallengereviewapp.service.ReviewService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ReviewController {
    private final ReviewService reviewService;

    public ReviewController(ReviewService reviewService) {
        this.reviewService = reviewService;
    }

    @GetMapping("/reviews/{id}")
    public Review getReview(@PathVariable String id) throws NotFoundException {
        return reviewService.getReview(id);
    }

    @GetMapping("/reviews")
    public List<Review> searchReview(@RequestParam(name = "q") String keyword) {
        return reviewService.searchReview(keyword);
    }

    @PutMapping("/reviews/{id}")
    public Review editReview(@PathVariable String id, @RequestBody Review updatedReview) throws NotFoundException {
        return reviewService.editReview(id, updatedReview);
    }
}
