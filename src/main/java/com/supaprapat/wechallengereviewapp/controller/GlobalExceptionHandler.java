package com.supaprapat.wechallengereviewapp.controller;

import com.supaprapat.wechallengereviewapp.exception.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.logging.Level;
import java.util.logging.Logger;

@ControllerAdvice
public class GlobalExceptionHandler {

    Logger logger = Logger.getLogger("GlobalExceptionHandler");

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity handleNotFoundException(NotFoundException e) {
        logger.log(Level.SEVERE, "Review is not found: " + e.getClass().getSimpleName());
        return new ResponseEntity(HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity handleException(Exception e) {
        logger.log(Level.SEVERE, "Internal server error: " + e.getClass().getSimpleName());
        return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
