package com.supaprapat.wechallengereviewapp.repository;

import com.supaprapat.wechallengereviewapp.model.Review;
import org.springframework.data.solr.repository.SolrCrudRepository;

import java.util.List;

public interface ReviewRepository extends SolrCrudRepository<Review, String> {

    List<Review> findByReviewContaining(String keyword);
}
