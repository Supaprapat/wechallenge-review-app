package com.supaprapat.wechallengereviewapp.repository;

import com.supaprapat.wechallengereviewapp.model.FoodDictionary;
import org.springframework.data.solr.repository.SolrCrudRepository;

import java.util.Optional;

public interface FoodDictionaryRepository extends SolrCrudRepository<FoodDictionary, String> {
    Optional<FoodDictionary> findByWord(String word);
}
