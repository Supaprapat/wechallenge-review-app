package com.supaprapat.wechallengereviewapp.service;

import com.supaprapat.wechallengereviewapp.exception.NotFoundException;
import com.supaprapat.wechallengereviewapp.model.FoodDictionary;
import com.supaprapat.wechallengereviewapp.model.Review;
import com.supaprapat.wechallengereviewapp.repository.FoodDictionaryRepository;
import com.supaprapat.wechallengereviewapp.repository.ReviewRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ReviewService {
    private final ReviewRepository reviewRepository;
    private final FoodDictionaryRepository foodDictionaryRepository;

    @Value("${highlight.tag}")
    private String highlightTag;

    public ReviewService(ReviewRepository reviewRepository, FoodDictionaryRepository foodDictionaryRepository) {
        this.reviewRepository = reviewRepository;
        this.foodDictionaryRepository = foodDictionaryRepository;
    }

    public Review getReview(String id) throws NotFoundException {
        return reviewRepository.findById(id).orElseThrow(NotFoundException::new);
    }

    public List<Review> searchReview(String keyword) {
        Optional<FoodDictionary> optionalFoodDictionary = foodDictionaryRepository.findByWord(keyword);
        if (optionalFoodDictionary.isPresent()) {
            return addHighlightTag(reviewRepository.findByReviewContaining(keyword), keyword);
        } else {
            return Collections.emptyList();
        }
    }

    public Review editReview(String id, Review updatedReview) throws NotFoundException {
        Review existingReview = reviewRepository.findById(id).orElseThrow(NotFoundException::new);

        existingReview.setReview(updatedReview.getReview());
        reviewRepository.save(existingReview);

        return existingReview;
    }

    private List<Review> addHighlightTag(List<Review> reviews, String keyword) {
        String keywordWithHighlight = String.format(highlightTag, keyword);
        return reviews.stream()
                .map(review -> new Review(review.getId(), review.getReview().replaceAll(keyword, keywordWithHighlight)))
                .collect(Collectors.toList());
    }
}
