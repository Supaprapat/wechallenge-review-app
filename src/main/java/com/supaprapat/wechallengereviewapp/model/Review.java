package com.supaprapat.wechallengereviewapp.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.solr.core.mapping.Indexed;
import org.springframework.data.solr.core.mapping.SolrDocument;

@Data
@AllArgsConstructor
@SolrDocument(collection = "Review")
public class Review {

    @Id
    @Indexed(name = "id", type = "string")
    private String id;

    @Indexed(name = "review", type = "string")
    private String review;
}
