package com.supaprapat.wechallengereviewapp.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.solr.core.mapping.Indexed;
import org.springframework.data.solr.core.mapping.SolrDocument;

@Data
@AllArgsConstructor
@SolrDocument(collection = "Food_dictionary")
public class FoodDictionary {

    @Id
    @Indexed(name = "word", type = "string")
    private String word;
}
