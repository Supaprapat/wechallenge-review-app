package com.supaprapat.wechallengereviewapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WechallengeReviewAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(WechallengeReviewAppApplication.class, args);
	}

}
