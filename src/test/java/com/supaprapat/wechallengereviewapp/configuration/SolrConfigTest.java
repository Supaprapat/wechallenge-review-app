package com.supaprapat.wechallengereviewapp.configuration;

import org.apache.solr.client.solrj.SolrClient;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.solr.core.SolrOperations;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class SolrConfigTest {
    private SolrConfig solrConfig;

    private final String solrUrl = "http://solr/solr";
    @Before
    public void setup() {
        solrConfig = new SolrConfig();
        ReflectionTestUtils.setField(solrConfig, "solrUrl", solrUrl);
    }

    @Test
    public void solrClient_success() {
        SolrClient actual = solrConfig.solrClient(solrUrl);
        assertNotNull(actual);
    }

    @Test
    public void solrTemplate_success() {
        SolrOperations actual = solrConfig.solrTemplate();
        assertNotNull(actual);
    }
}
