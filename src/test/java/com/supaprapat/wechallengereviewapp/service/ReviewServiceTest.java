package com.supaprapat.wechallengereviewapp.service;

import com.supaprapat.wechallengereviewapp.exception.NotFoundException;
import com.supaprapat.wechallengereviewapp.model.FoodDictionary;
import com.supaprapat.wechallengereviewapp.model.Review;
import com.supaprapat.wechallengereviewapp.repository.FoodDictionaryRepository;
import com.supaprapat.wechallengereviewapp.repository.ReviewRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ReviewServiceTest {
    private ReviewService reviewService;

    @Mock
    private ReviewRepository reviewRepository;

    @Mock
    private FoodDictionaryRepository foodDictionaryRepository;

    private final String reviewId = "1";
    private final String reviewText = "a review";
    private final String keyword = "review";
    private final String highlightedReview = "a <keyword>review</keyword>";
    private final String updatedReviewText = "an updated review";

    @Before
    public void setup() {
        reviewService = new ReviewService(reviewRepository, foodDictionaryRepository);

        ReflectionTestUtils.setField(reviewService, "highlightTag", "<keyword>%s</keyword>");
        when(reviewRepository.findById(reviewId)).thenReturn(Optional.of(new Review(reviewId, reviewText)));
        when(foodDictionaryRepository.findByWord(keyword)).thenReturn(Optional.of(new FoodDictionary(keyword)));
        when(reviewRepository.findByReviewContaining(keyword)).thenReturn(Collections.singletonList(new Review(reviewId, reviewText)));
        when(reviewRepository.save(any(Review.class))).thenReturn(null);
    }

    @Test
    public void getReview_success() throws NotFoundException {
        Review actual = reviewService.getReview(reviewId);
        assertEquals(reviewId, actual.getId());
        assertEquals(reviewText, actual.getReview());
    }

    @Test
    public void searchReview_success() {
        List<Review> actual = reviewService.searchReview(keyword);
        assertEquals(1, actual.size());
        assertEquals(reviewId, actual.get(0).getId());
        assertEquals(highlightedReview, actual.get(0).getReview());
    }

    @Test
    public void searchReview_notFoundInFoodDictionary() {
        when(foodDictionaryRepository.findByWord(keyword)).thenReturn(Optional.empty());
        List<Review> actual = reviewService.searchReview(keyword);
        assertEquals(0, actual.size());
    }

    @Test
    public void editReview_success() throws NotFoundException {
        Review actual = reviewService.editReview(reviewId, new Review(null, updatedReviewText));
        assertEquals(reviewId, actual.getId());
        assertEquals(updatedReviewText, actual.getReview());
    }
}
