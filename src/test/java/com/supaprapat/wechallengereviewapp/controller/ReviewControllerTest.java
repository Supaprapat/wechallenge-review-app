package com.supaprapat.wechallengereviewapp.controller;

import com.supaprapat.wechallengereviewapp.exception.NotFoundException;
import com.supaprapat.wechallengereviewapp.model.Review;
import com.supaprapat.wechallengereviewapp.service.ReviewService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ReviewControllerTest {
    private ReviewController reviewController;

    @Mock
    private ReviewService reviewService;

    private final String reviewId = "1";
    private final String reviewText = "a review";
    private final String keyword = "review";
    private final String highlightedReview = "a <keyword>review</keyword>";
    private final String updatedReviewText = "an updated review";

    @Before
    public void setup() throws NotFoundException {
        reviewController = new ReviewController(reviewService);

        when(reviewService.getReview(reviewId)).thenReturn(new Review(reviewId, reviewText));
        when(reviewService.searchReview(keyword)).thenReturn(Collections.singletonList(new Review(reviewId, highlightedReview)));
        when(reviewService.editReview(eq(reviewId), any(Review.class))).thenReturn(new Review(reviewId, updatedReviewText));
    }

    @Test
    public void getReview_success() throws NotFoundException {
        Review actual = reviewController.getReview(reviewId);
        assertEquals(reviewId, actual.getId());
        assertEquals(reviewText, actual.getReview());
    }

    @Test
    public void searchReview_success() {
        List<Review> actual = reviewController.searchReview(keyword);
        assertEquals(1, actual.size());
        assertEquals(reviewId, actual.get(0).getId());
        assertEquals(highlightedReview, actual.get(0).getReview());
    }

    @Test
    public void editReview_success() throws NotFoundException {
        Review actual = reviewController.editReview(reviewId, new Review(null, updatedReviewText));
        assertEquals(reviewId, actual.getId());
        assertEquals(updatedReviewText, actual.getReview());
    }
}
