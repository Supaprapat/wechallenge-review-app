package com.supaprapat.wechallengereviewapp.controller;

import com.supaprapat.wechallengereviewapp.exception.NotFoundException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class GlobalExceptionHandlerTest {
    private GlobalExceptionHandler globalExceptionHandler;

    @Before
    public void setup() {
        globalExceptionHandler = new GlobalExceptionHandler();
    }

    @Test
    public void handleNotFoundException_success() {
        ResponseEntity actual = globalExceptionHandler.handleNotFoundException(new NotFoundException());
        assertEquals(HttpStatus.NOT_FOUND, actual.getStatusCode());
    }

    @Test
    public void handleException_success() {
        ResponseEntity actual = globalExceptionHandler.handleException(new Exception());
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, actual.getStatusCode());
    }
}
